import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('Wallet Nominal Form/XCUIElementTypeImage - arrow down sumber dana'), 0)

Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeImage - arrow down sumber dana'), 0)

CustomKeywords.'screenshot.capture.Screenshot'()

text1 = "0"

text2 = "2"

device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.70
int endY = device_Height * 0.30

if(Mobile.verifyElementExist(findTestObject('Wallet Nominal Form/XCUIElementTypeCell - cell sumber dana N', [('text') : "$text2"]), 0, FailureHandling.OPTIONAL) == true) {
	
	Mobile.swipe(startX, startY, endX, endY)
	
	Mobile.delay(3)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
	startY = device_Height * 0.30
	endY = device_Height * 0.70
	
	Mobile.swipe(startX, startY, endX, endY)
	
	Mobile.delay(2)
	
	startY = device_Height * 0.85
	
	Mobile.tapAtPosition(startX, startY)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
} else {
	Mobile.verifyElementExist(findTestObject('Wallet Nominal Form/XCUIElementTypeCell - cell sumber dana N', [('text') : "$text1"]), 0)
	
	Mobile.delay(2)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
	startY = device_Height * 0.6
	
	Mobile.tapAtPosition(startX, startY)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
}