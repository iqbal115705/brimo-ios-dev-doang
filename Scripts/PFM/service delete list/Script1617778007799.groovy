import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

pages = page.toString()

switch(pages) {
	case 'pengeluaran':
		Mobile.verifyElementVisible(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - Detail Pengeluaran'), 0)

		break;

	case 'pemasukan':
		Mobile.verifyElementVisible(findTestObject('PFM/detail pemasukan/XCUIElementTypeStaticText - Detail Pemasukan'), 0)
		break;

}

if(Mobile.verifyElementNotVisible(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - No Ref'), 10, FailureHandling.OPTIONAL) == true) {

	Mobile.tap(findTestObject('PFM/detail pengeluaran/XCUIElementTypeButton - Hapus'), 0)

	Mobile.verifyElementVisible(findTestObject('PFM/delete pengeluaran/XCUIElementTypeStaticText - Hapus Catatan'), 0)

	CustomKeywords.'screenshot.capture.Screenshot'()

	Mobile.tap(findTestObject('PFM/delete pengeluaran/XCUIElementTypeButton - Ya'), 0)

	CustomKeywords.'screenshot.capture.Screenshot'()

} else {

	Mobile.tap(findTestObject('PFM/detail pengeluaran/XCUIElementTypeButton - Hapus'), 0)

	Mobile.verifyElementVisible(findTestObject('PFM/delete pengeluaran/XCUIElementTypeStaticText - Maaf'), 0)

	CustomKeywords.'screenshot.capture.Screenshot'()

	Mobile.tap(findTestObject('PFM/delete pengeluaran/XCUIElementTypeButton - OK'), 0)

}