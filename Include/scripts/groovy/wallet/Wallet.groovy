package wallet
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import database.methods

class Wallet {
	@Given ("Costum")
	def Costum() {
		WebUI.callTestCase(findTestCase('General/Database Connect'), [:], FailureHandling.STOP_ON_FAILURE)

		//salah
		CustomKeywords.'database.methods.executeUpdate'(('UPDATE tbl_history_purchase_ewallet SET value = "" WHERE username = "' + username.toString()) + '"')

		//bener
		def db = new methods()
		db.executeUpdate('UPDATE tbl_history_purchase_ewallet SET value = "2" WHERE username = "' + "brimosv004" + '"')




		WebUI.callTestCase(findTestCase('General/Database Close'), [:], FailureHandling.STOP_ON_FAILURE)


		//		Mobile.startExistingApplication('id.co.bri.brimo')
		Mobile.delay(30)
		//		Mobile.tap(findTestObject('Fast Menu/XCUIElementTypeButton - Login'), 0)
		//		Mobile.setText(findTestObject('Login Form/XCUIElementTypeTextField - Username'), "brimosv004", 0)
		//		Mobile.tap(findTestObject('Login Form/XCUIElementTypeImage - BrimoLoginIlustration'), 0)
		//		Mobile.setText(findTestObject('Login Form/XCUIElementTypeSecureTextField - Password'), "Jakarta123", 0)
		//		Mobile.tap(findTestObject('Login Form/XCUIElementTypeImage - BrimoLoginIlustration'), 0)
		//		Mobile.tap(findTestObject('Login Form/XCUIElementTypeButton - Login'), 0);Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Saldo Rekening Utama'), 0)
		//		Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 0);Mobile.verifyElementExist(findTestObject('Wallet Form/XCUIElementTypeButton - Top Up Baru'), 0)
		//		Mobile.tap(findTestObject('Wallet Form/XCUIElementTypeButton - Top Up Baru'), 0)
		//		Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - BrimoIconArrowDownOutline'), 0)
		////		Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeStaticText - LinkAja'), 0)
		//		Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeStaticText - OVO'), 0)
		////		Mobile.setText(findTestObject('Wallet New Form/XCUIElementTypeTextField - Nomor Tujuan'), '085691335269', 0)
		//		Mobile.setText(findTestObject('Wallet New Form/XCUIElementTypeTextField - Nomor Tujuan'), '081218022786', 0)
		//		Mobile.tap(findTestObject('Wallet New Form/Hide Keyboard halaman Top Up Baru'), 0)
		//		Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Lanjutkan'), 0)
	}

	@When ('I want to wallet from fast menu')
	def I_want_go_to_wallet_from_fast_menu() {
		Mobile.callTestCase(findTestCase('Wallet/Go To Wallet From Fast Menu'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And('Click Dompet Digital')
	def Click_Dompet_Digital(){
		Mobile.callTestCase(findTestCase('Wallet/Go To Wallet From Dashboard'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I saw my wallet form')
	def I_saw_my_wallet_page(){
		Mobile.callTestCase(findTestCase('Wallet/Validate/1 - Wallet Form/Wallet Form'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I saw my wallet new')
	def I_saw_my_wallet_new(){
		Mobile.callTestCase(findTestCase('Wallet/Validate/2 - Wallet New Form/Wallet New Form'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I saw my wallet norminal')
	def I_saw_my_wallet_norminal(){
		Mobile.callTestCase(findTestCase('Wallet/Service Wallet Nominal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I saw my top up wallet history')
	def I_saw_my_top_up_wallet_history(){
		Mobile.callTestCase(findTestCase('Wallet/Service Wallet Form'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I can search my wallet list')
	def I_can_search_my_wallet_list(){
		Mobile.callTestCase(findTestCase('Wallet/Validate/1 - Wallet Form/Validate Field Cari Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I want to add recipient of wallet')
	def I_want_to_add_recipient_of_wallet(){
		Mobile.callTestCase(findTestCase('Wallet/Go To Wallet New From Wallet Form'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I try adding recipient of wallet in condition with (.*), (.*) and (.*)')
	def I_try_adding_recipient_of_wallet_in_condition_with_wallet_type_and_walletNumber(String wallet, String type, String walletNumber){
		Mobile.callTestCase(findTestCase('Wallet/Service Wallet New Form'), [('wallet') : wallet, ('type') : type, ('walletNumber') : walletNumber], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I inputting (.*) for the wallet amount and (.*) with (.*) , then choose account (.*)')
	def I_inputting_amount_for_the_transfer_amount(String amountWallet, String decision, String name, String debit){
		Mobile.callTestCase(findTestCase('Wallet/Service Wallet Nominal'), [('amountWallet') : amountWallet, ('decision') : decision, ('name') : name, ('debit') : debit], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I confirm top up wallet with (.*) and (.*)')
	def I_confirm_top_up_wallet_with_detail(String detail, String wallet){
		Mobile.callTestCase(findTestCase('Wallet/Service Wallet Confirm'), [('detail') : detail, ('wallet') : wallet], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I top up wallet from my list')
	def I_top_up_wallet_from_my_list(){
		Mobile.callTestCase(findTestCase('Wallet/Service Wallet From List'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I top up wallet from my history')
	def I_top_up_wallet_from_my_history(){
		Mobile.callTestCase(findTestCase('Wallet/Service Wallet From History'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then('Abnormal Saldo - I inputting (.*) for the wallet amount with (.*)')
	def Abnormal_saldo_i_inputting_amount_for_the_transfer_amount(String amountWallet, String username){
		Mobile.callTestCase(findTestCase('Wallet/Service Wallet Nominal - Abnormal Saldo'), [('amountWallet') : amountWallet, ('username') : username], FailureHandling.STOP_ON_FAILURE)
	}
	
// iqbal function
	
	@And('I want to top up my wallet with (.*) for (.*)')
	def I_want_to_top_up_my_wallet(String condition, String username){
		//Mobile.callTestCase(findTestCase('Wallet/Go To Wallet From Dashboard'), [('condition') : condition, ('username') : username], FailureHandling.STOP_ON_FAILURE)
		Mobile.callTestCase(findTestCase('Wallet2/Go To Wallet From Dashboard with Condition'), [('condition') : condition, ('username') : username], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I want saw my top up wallet history')
	def I_want_saw_my_top_up_wallet_history(){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet Form'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I will to add recipient of wallet')
	def I_will_add_recipient_of_wallet(){
		Mobile.callTestCase(findTestCase('Wallet2/Go To Wallet New From Wallet Form'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I insist adding recipient of wallet in condition with (.*), (.*) and (.*)')
	def I_insist_adding_recipient_of_wallet_in_condition_with_wallet_type_and_walletNumber(String wallet, String type, String walletNumber){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet New Form'), [('wallet') : wallet, ('type') : type, ('walletNumber') : walletNumber], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I input (.*) for the wallet amount and (.*) with (.*) , then choose account (.*)')
	def I_input_amount_for_the_transfer_amount(String amountWallet, String decision, String name, String debit){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet Nominal'), [('amountWallet') : amountWallet, ('decision') : decision, ('name') : name, ('debit') : debit], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I want confirm top up wallet with (.*) and (.*)')
	def I_want_confirm_top_up_wallet_with_detail(String detail, String wallet){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet Confirm'), [('detail') : detail, ('wallet') : wallet], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I want to top up wallet from my list')
	def I_want_to_top_up_wallet_from_my_list(){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet From List'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I want to top up wallet from my history')
	def I_want_to_top_up_wallet_from_my_history(){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet From History'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then('an Abnormal Saldo - I inputting (.*) for the wallet amount with (.*)')
	def an_Abnormal_saldo_i_inputting_amount_for_the_transfer_amount(String amountWallet, String username){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet Nominal - Abnormal Saldo'), [('amountWallet') : amountWallet, ('username') : username], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I want to verify search for wallet list when internet is (.*)')
	def I_verify_search_for_Wallet_List_when_internet_is(String connection) {
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet Search List'), [('connection') : connection], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I adding recipient of wallet with (.*) and (.*)')
	def I_adding_recipient_of_wallet_with_wallet_and_type(String wallet, String type){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet New Form no Input'), [('wallet') : wallet, ('type') : type], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I add (.*) of recipient by input')
	def I_add_wallet_number_of_recipient_by_input(String walletNumber){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet New Form Input Number'), [('walletNumber') : walletNumber], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I add (.*) of recipient by contact')
	def I_add_wallet_number_of_recipient_by_contact(String contactName){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet New Form Contact Number'), [('contactName') : contactName], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I go to nominal page with (.*) condition')
	def i_go_to_nominal_page_with_condition(String page_condition) {
		Mobile.callTestCase(findTestCase('Wallet2/Go To Nominal With Condition'), [('page_condition') : page_condition], FailureHandling.STOP_ON_FAILURE)
	}

	@Then('I success go to nominal page and see status checkbox simpan sebagai')
	def i_success_go_to_nominal_page_to_see_status_checkbox() {
		Mobile.callTestCase(findTestCase('Wallet2/Detailing/Nominal Page Checkbox'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I verify field input nama is enable after tap checkbox simpan')
	def i_verify_field_input_nama_after_tap_checkbox_simpan() {
		Mobile.callTestCase(findTestCase('Wallet2/Detailing/Nominal Page Verify Checkbox'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I input valid (.*) for wallet')
	def i_input_valid_amount_for_wallet(String walletAmount) {
		Mobile.callTestCase(findTestCase('Wallet2/Detailing/Nominal Page Input Valid Nominal'), [('walletAmount') : walletAmount], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I input invalid amount for wallet in (.*)')
	def i_input_invalid_amount_for_wallet_in_terms(String terms) {
		Mobile.callTestCase(findTestCase('Wallet2/Detailing/Nominal Page Input Invalid Nominal'), [('terms') : terms], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I top up wallet from my list with (.*) fund for (.*)')
	def I_top_up_wallet_from_my_list_with_fund_for_user(String funds, String username){
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet From List with Condition'), [('funds') : funds, ('username') : username], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I want to verify the account list')
	def i_want_to_verify_the_account_list() {
		Mobile.callTestCase(findTestCase('Wallet2/Detailing/Nominal Page Verify Account List'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I verify nominal page when (.*) condition')
	def i_verify_nominal_page_when_condition(String page_condition) {
		Mobile.callTestCase(findTestCase('Wallet2/Detailing/Nominal Page Condition'), [('page_condition'): page_condition], FailureHandling.STOP_ON_FAILURE)
	}

	@And('I tap top up button')
	def i_tap_top_up_button() {
		Mobile.tap(findTestObject('Object Repository/Wallet Nominal Form/XCUIElementTypeButton - Top Up'), 0)
	}

	@Then('I successfully go to confirmation page')
	def i_successfully_go_to_confirmation_page() {
		Mobile.callTestCase(findTestCase('Wallet2/Go To Confirmation Page from Wallet'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I check the fee for top up wallet')
	def i_check_the_fee_for_top_up_wallet() {
		Mobile.callTestCase(findTestCase('Wallet2/Service Wallet Confirm Verify Fee'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then('I successfully in input PIN page')
	def i_successfully_in_input_PIN_page() {
		Mobile.callTestCase(findTestCase('Pin/Pin Verify Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I input (.*) for (.*) condition')
	def i_input_pin_for_status_condition(String PIN, String status) {
		Mobile.callTestCase(findTestCase('Pin/Pin Confirm with Condition'), [('PIN') : PIN, ('status') : status], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I set up the (.*) condition for this page and input valid (.*)')
	def i_set_up_the_page_condition_for_this_page(String page_condition, String PIN) {
		Mobile.callTestCase(findTestCase('Pin/Pin with Page Condition'), [('page_condition') : page_condition, ('PIN') : PIN], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I input valid amount for wallet and choose (.*) account')
	def I_input_valid_amount_for_wallet_and_choose_account(String accounts) {
		Mobile.callTestCase(findTestCase('Wallet2/Detailing/Nominal Page Verify for Freeze Dormant Closed Account'), [('accounts') : accounts], FailureHandling.STOP_ON_FAILURE)
	}

	@Then('I unsuccessful go to bukti transaksi page')
	def i_unsuccessful_go_to_bukti_transaksi_page() {
		Mobile.callTestCase(findTestCase('Wallet2/Bukti Transaksi/Bukti Transaksi Verify Not Success'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then('I successful go to bukti transaksi page')
	def i_successful_go_to_bukti_transaksi_page() {
		Mobile.callTestCase(findTestCase('Wallet2/Bukti Transaksi/Bukti Transaksi Verify Success'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When('I verify bukti transaksi page in (.*) condition')
	def i_verify_bukti_transaksi_page_in_condition(String page_condition) {
		Mobile.callTestCase(findTestCase('Wallet2/Bukti Transaksi/Bukti Transaksi Verify with Condition'), [('page_condition') : page_condition], FailureHandling.STOP_ON_FAILURE)
	}
}