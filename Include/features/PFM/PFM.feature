#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: personal finance management
  I want to use this Catatan Keuangan or PFM feature

  @tag1
  Scenario Outline: verify spending, earning and report are empty (tc 101, 201 & 301)
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    Then I verify the <page> page there is no record yet
    

    Examples: 
      | 	page  		|
      |	pengeluaran	|
      |	pemasukan		|
      |	laporan			|
      
  @tag1b
  Scenario Outline: verify spending, earning and report are empty (tc 101, 201 & 301)
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    Then I verify the <page> page there is no record yet
    And I verify the <range> range in laporan page
    

    Examples: 
      | 	page  		|	range			|
      |	laporan			| bulan ini|
      |	laporan			| bulan lalu|
      |	laporan			| 3 bulan|
      
      
  @tag2
  Scenario Outline: check jumlah in some condition
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I go to tambah catatan for <page>
    And I try input to verify <amount>

    Examples: 
      | 	page  		|	amount 			|
      |	pengeluaran	| 12345678910	|
      #|	pengeluaran	| 0000000000	|
      #|	pemasukan		| 12345678910	|
      #|	pemasukan		| 0000000000	|
      
  @tag3
  Scenario Outline: check tanggal in some condition
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I go to tambah catatan for <page>
    And I try choose <date> date

    Examples: 
      | 	page  		|	date			|
      |	pengeluaran	| today			|
      |	pengeluaran	| yesterday	|
      |	pengeluaran	| tomorrow	|
      |	pemasukan		| today			|
      |	pemasukan		| yesterday	|
      |	pemasukan		| tomorrow	|
      
      
  @tag4
  Scenario Outline: add catatan pengeluaran baru
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I go to tambah catatan for <page>
    And I input jumlah <amount>
    And I try choose <date> date
    And I choose for <category>
    And I choose pembayaran for <payment>
    And I input note <note>
    And I tap simpan button
    Then I verify the input <category> is listed
    When I verify the detail of spending which contain <category>, <note>, <amount>, <date> and <payment>
    And I verify the spending is listed in laporan by <category>

    Examples: 
      | 	page  		| amount		|date				| category																|	payment						| note										|
      #|	pengeluaran	| 12100		 	|today			| Makanan & Minuman												| Tunai							|	ini pengeluaran Makan|
      |	pengeluaran	| 12200		 	|today			| Transportasi Online											| Debit							| ini pengeluaran ojol|
      #|	pengeluaran	| 12300		 	|today			| Transportasi 														| Dompet Digital		|	ini pengeluaran ojek|
      #|	pengeluaran	| 12400		 	|yesterday	| Dompet Digital													| Kartu Kredit			|	ini pengeluaran Wallet|
      #|	pengeluaran	| 12500		 	|today			| Kebutuhan Bulanan												| Tunai							|	ini pengeluaran bulanan|
      #|	pengeluaran	| 12600		 	|today			| Belanja																	| Debit							|	ini pengeluaran Belanja|
      #|	pengeluaran	| 12700		 	|today			| Sewa/Kontrak														| Dompet Digital		|	ini pengeluaran Sewa|
      #|	pengeluaran	| 12800		 	|today			| Cicilan																	| Kartu Kredit			|	ini pengeluaran cicilan|
      #|	pengeluaran	| 12900		 	|today			| Entertainment														| Tunai							|	ini pengeluaran hiburan|
      #|	pengeluaran	| 13000		 	|today			| Hobi																		| Debit							|	ini pengeluaran hobi|
      #|	pengeluaran	| 13100		 	|today			| Olahraga																| Dompet Digital		|	ini pengeluaran olahraga|
      #|	pengeluaran	| 13200		 	|today			| Kesehatan																| Kartu Kredit			|	ini pengeluaran medis|
      #|	pengeluaran	| 13300		 	|today			| Pendidikan															| Tunai							|	ini pengeluaran sekolah|
      #|	pengeluaran	| 13400		 	|today			| Asuransi																| Debit							|	ini pengeluaran asuransi|
      #|	pengeluaran	| 13500		 	|today			| Investasi																| Dompet Digital		|	ini pengeluaran invest|
      #|	pengeluaran	| 13600		 	|today			| Amal/Donasi															| Kartu Kredit			|	ini pengeluaran sedekah|
      #|	pengeluaran	| 13700		 	|today			| Uang Keluar															| Tunai							|	ini pengeluaran khilaf|
      #|	pemasukan		| 
      
      
  @tag5
  Scenario Outline: edit catatan pengeluaran which have reference number
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for a reference number
    And I try edit list
   	And I edit category for <new_category>
   	And I edit note for <page>
   	And I tap simpan button
    
    Examples: 
      | 	page  		|	new_category					|
      |	pengeluaran	|	Uang Keluar|
      
  @tag6
  Scenario Outline: delete catatan pengeluaran which have reference number
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for a reference number
    And I try delete list for <page>
    
    Examples: 
      | 	page  		|
      |	pengeluaran	|
      
  @tag7
  Scenario Outline: edit catatan pengeluaran which have no reference number
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for have no reference number
    And I try edit list
    And I edit jumlah <amount>
    And I edit category for <new_category>
    And I try edit <date> date
    And I edit pembayaran for <payment>
   	And I edit note for <page>
   	And I tap simpan button
    
    Examples: 
      | 	page  		|	amount	|	date	|	payment	|	new_category					|
      |	pengeluaran	|	11000		|	today	|	Tunai		|	Uang Keluar|
      
      
   @tag8
  Scenario Outline: delete catatan pengeluaran which have no reference number
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for have no reference number
    And I try delete list for <page>
    
    Examples: 
      | 	page  		|
      |	pengeluaran	|
      
  @tag9
  Scenario Outline: add catatan pemasukan baru
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I go to tambah catatan for <page>
    And I input jumlah <amount>
    And I try choose <date> date
    And I choose for <category>
    And I input pemasukan note <note>
    And I tap simpan button
    Then I verify the input <category> is listed
    When I verify the detail of earning which contain <category>, <note>, <amount> and <date>
    And I verify the earning is listed in laporan page

    Examples: 
      | 	page  		| amount		|date				| category						| note										|
      |	pemasukan		| 12100		 	|today			| Gaji								|	ini pemasukan gaji|
      |	pemasukan		| 12100		 	|today			| Usaha								|	ini pemasukan usaha|
      |	pemasukan		| 12100		 	|today			| Bonus								|	ini pemasukan bonus|
      |	pemasukan		| 12100		 	|today			| Bunga								|	ini pemasukan bunga|
      |	pemasukan		| 12100		 	|today			| Hadiah							|	ini pemasukan hadiah|
      |	pemasukan		| 12100		 	|today			| Uang Masuk					|	ini pemasukan uang masuk|
  
  @tag10
  Scenario Outline: edit catatan pemasukan
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for have no reference number
    And I try edit list
    And I edit jumlah <amount>
    And I edit category for <new_category>
    And I try edit <date> date
   	And I edit note for <page>
   	And I tap simpan button
    
    Examples: 
      | 	page  		|	amount	|	date	|	new_category					|
      |	pemasukan		|	11000		|	today	|	Uang Masuk|
  
  @tag11
  Scenario Outline: delete catatan pemasukan
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for have no reference number
    And I try delete list for <page>
    
    Examples: 
      | 	page  		|
      |	pemasukan		|