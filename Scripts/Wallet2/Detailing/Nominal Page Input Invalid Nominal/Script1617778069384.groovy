import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.apache.commons.lang.StringUtils
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.util.KeywordUtil

if(terms.toString() == 'limit wallet') {
	
	Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nominal'), '2010000', 0)
	
	Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Nominal'), 0)
	
	Mobile.tap(findTestObject('Wallet Confirm/XCUIElementTypeButton - Top Up'), 0)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	
	Mobile.verifyElementExist(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - messageLabel'), 0)
	
} else if(terms.toString() == 'over balance') {
	String balance = Mobile.getText(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - saldo rekening utama'), 0)
	
	Mobile.comment('ini balace = ' + balance)
	
	//hapus setelah ,
	String balanceTemp = StringUtils.substringBefore(balance, ",")
	//end hapus
	
	Mobile.comment('ini balace = ' + balanceTemp)
	
	String trimBalance = balanceTemp.replaceAll("\\D+", "")
	
	Mobile.comment(trimBalance)
	
	long balanceInt = Long.parseLong(trimBalance)
	
	Mobile.comment(balanceInt.toString())
	
	long moreBalance = balanceInt + 100000
	
	Mobile.comment(moreBalance.toString())
	
	Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nominal'), moreBalance.toString() , 0)
	
	Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Nominal'), 0)
	
	Mobile.verifyElementVisible(findTestObject('Object Repository/Wallet Nominal Form/XCUIElementTypeStaticText - Saldo Anda tidak mencukupi'), 12)
	
	CustomKeywords.'screenshot.capture.Screenshot'()
	 	
} else {
	String[] arrayAmount = [ "0", "9000","00000","1234567891011"]
	
	for(int i = 0; i <= arrayAmount.length - 1; i++) {
		if(i != 3) {
			Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nominal'), arrayAmount[i].toString() , 0)
			
			Mobile.verifyElementExist(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Minimal Top Up Rp10.000'), 10)
			
			Mobile.verifyElementAttributeValue(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Top Up'), 'enabled', 'false', 0)
			
			KeywordUtil.markPassed('Rp.' +arrayAmount[i].toString() + ' Dibawah minimal top up tidak dapat diproses')
			
			Mobile.comment('Rp.' +arrayAmount[i].toString() + ' Dibawah minimal top up tidak dapat diproses')
			
			CustomKeywords.'screenshot.capture.Screenshot'()
			
		} else {
			Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nominal'), arrayAmount[i] , 0)
			
			Mobile.verifyElementNotExist(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Minimal Top Up Rp10.000'), 10)
			
			String amount = Mobile.getText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nominal'), 0)
			
			String trimAmount = amount.replaceAll("\\D+", "")
			
			Mobile.comment(trimAmount)
			
			if(trimAmount.length() <= 12) {
				if(Mobile.verifyElementVisible(findTestObject('Object Repository/Wallet Nominal Form/XCUIElementTypeStaticText - Saldo Anda tidak mencukupi'), 10, FailureHandling.OPTIONAL) == true) {
					CustomKeywords.'screenshot.capture.Screenshot'()
					Mobile.comment('Digit maksimal adalah 12 digit')
					KeywordUtil.markPassed('Digit maksimal adalah 12 digit')
				} else {
					CustomKeywords.'screenshot.capture.Screenshot'()
					Mobile.comment('Rp.' +trimAmount.toString() + ' Digit maksimal adalah 12 digit')
					KeywordUtil.markPassed('Rp.' +trimAmount.toString() + ' Digit maksimal adalah 12 digit')
				}
			} else {
				CustomKeywords.'screenshot.capture.Screenshot'()
				Mobile.comment('Rp.' +arrayAmount[i].toString() + ' ini tidak lolos karena 13 angka')
				KeywordUtil.markFailedAndStop('Rp.' +arrayAmount[i].toString() + ' ini tidak lolos karena 13 angka')
			}
		}
	}
}