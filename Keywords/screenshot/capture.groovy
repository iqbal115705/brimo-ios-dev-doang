package screenshot
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.configuration.RunConfiguration

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile


class capture {
	/**
	 * Check if element present in timeout
	 * @param to Katalon test object
	 * @param timeout time to wait for element to show up
	 * @return true if element present, otherwise false
	 */
	@Keyword
	def Screenshot(){

		String filePath = RunConfiguration.getProjectDir()

		Date today = new Date()

		String todaysDate = today.format('MM_dd_yy')

		String nowTime = today.format('hh_mm_ss')

		//Mobile.takeScreenshot((((filePath + '/Screenshot/iOS2/Katalon Picture_' + todaysDate) + '-')+ nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)

		Mobile.takeScreenshotAsCheckpoint((((filePath + '/Screenshot/iOS2/Katalon Picture_' + todaysDate) + '-')+ nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)

		//Mobile.takeScreenshot((((('/Users/mochamadiqbal/Documents/BRI/Brimo Native iOS/Screenshot/iOS/Katalon Picture_' + todaysDate) + '-')+ nowTime) + name) + '.png', FailureHandling.STOP_ON_FAILURE)

		//check
	}

	def Screenshot(String name){

		String filePath = RunConfiguration.getProjectDir()

		Date today = new Date()

		String todaysDate = today.format('MM_dd_yy')

		String nowTime = today.format('hh_mm_ss')

		Mobile.takeScreenshotAsCheckpoint((((filePath + '/Screenshot/iOS2/Katalon Picture_'+ name + todaysDate) + '-')+ nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)
	}
}